# TP2 : On va router des trucs

## I. ARP

### 1. Echange ARP

#### Générer des requêtes ARP

- effectuer un ping d'une machine à l'autre

```
$ ping 10.2.1.12

PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.53 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.634 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=1.43 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.590 ms
```

- observer les tables ARP des deux machines
- repérer l'adresse MAC de node1 dans la table ARP de node2 et vice-versa

```
$ ip neigh show dev ens34

10.2.1.1 lladdr 00:50:56:c0:00:02 DELAY
10.2.1.12 lladdr 00:50:56:27:41:51 STALE
```

L'adresse MAC de `node2` est `00:50:56:27:41:51`, on la repère grâce à l'adresse ip `10.2.1.12`

```
ip neigh show dev ens34

10.2.1.11 lladdr 00:50:56:33:ac:13 STALE
10.2.1.1 lladdr 00:50:56:c0:00:02 REACHABLE
```

Et vice-versa l'adresse MAC de `node1` est `00:50:56:c0:00:02`

```
$ ip a

[...]
2: ens34: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:50:56:27:41:51 brd ff:ff:ff:ff:ff:ff
    [...]
```

On repère l'adresse MAC sur `node2` avec la commande `ip a` et elle correspond bien à ce qu'on a trouvé précedemment.

### 2. Analyse de trames

#### Analyse de trames

- utilisez la commande tcpdump pour réaliser une capture de trame

    ```
    $ sudo tcpdump -i ens34 -w ma_capture.pcap not port 22
    ```

- videz vos tables ARP, sur les deux machines, puis effectuez un ping

    ```
    $ sudo ip neigh flush all
    $ ping 10.2.1.12

    PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
    64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=1.20 ms
    64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.767 ms
    64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.762 ms
    64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.755 ms
    ```

- Écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, je veux TOUTES les trames
    - uniquement l'ARP
    - sans doublons de trame, dans le cas où vous avez des trames en double

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | VMware_27:41:51          | Broadcast                  |
| 2     | Réponse ARP | VMware_c0:00:02          | VMware_27:41:51            |
| 3     | Requête ARP | VMware_33:ac:13          | Broadcast                  |
| 4     | Réponse ARP | VMware_c0:00:02          | VMware_33:ac:13            |
| 11    | Requête ARP | VMware_33:ac:13          | Broadcast                  |
| 12    | Réponse ARP | VMware_27:41:51          | VMware_33:ac:13            |
| 21    | Requête ARP | VMware_27:41:51          | VMware_33:ac:13            |
| 22    | Réponse ARP | VMware_33:ac:13          | VMware_27:41:51            |


[📁 Capture réseau tp2_arp.pcap](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP2/tp2_arp.pcap)

## II. Routage

### 1. Mise en place du routage

#### Activer le routage sur le noeud `router.net2.tp2`

```
$ sudo nano /etc/sysctl.d/router.conf
```
```
net.ipv4.ip_forward = 1
```
```
$ sudo sysctl -p /etc/sysctl.d/router.conf
```
#### Ajouter les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se ping

- il faut ajouter une seule route des deux côtés
- une fois les routes en place, vérifiez avec un ping que les deux machines peuvent se joindre

    - sur `node1` : 

		```
		$ sudo nano /etc/sysconfig/network-scripts/route-ens34
		```
		```
		10.2.2.0/24 via 10.2.1.254 dev ens34
		```
		`$ reboot`
		```
		$ ip r s
		
	    10.2.1.0/24 dev ens34 proto kernel scope link src 10.2.1.11 metric 100
		10.2.2.0/24 via 10.2.1.254 dev ens34 proto static metric 100
		```
		```
		$ ping 10.2.2.12
	
		PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
		64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.05 ms
		64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.37 ms
		64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.51 ms
		64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.76 ms
		```

    - sur `marcel` : 


		```
		$ sudo nano /etc/sysconfig/network-scripts/route-ens34
		```
		```
		10.2.1.0/24 via 10.2.2.254 dev ens34
		```
		`reboot`
		```
		$ ip r s
		
		10.2.1.0/24 via 10.2.2.254 dev ens34 proto static metric 100
		10.2.2.0/24 dev ens34 proto kernel scope link src 10.2.2.12 metric 100
		```
		```
		$ ping 10.2.1.11
		
		PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
		64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.54 ms
		64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.48 ms
		64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.46 ms
		64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.62 ms
		```
        
### 2. Analyse de trames

#### Analyse des échanges ARP

- videz les tables ARP des trois noeuds

    `sudo ip neigh flush all`

- effectuez un ping de node1.net1.tp2 vers marcel.net2.tp2

    ```
    $ ping 10.2.2.12

    PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
    64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.74 ms
    64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.45 ms
    64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.43 ms
    64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.48 ms
    ```

- regardez les tables ARP des trois noeuds

    ```
    node1$ ip n s

    10.2.1.254 dev ens34 lladdr 00:50:56:31:95:42 STALE
    10.2.1.1 dev ens34 lladdr 00:50:56:c0:00:02 REACHABLE
    ```
    ```
    router$ ip n s

    10.2.1.1 dev ens34 lladdr 00:50:56:c0:00:02 REACHABLE
    10.2.2.12 dev ens38 lladdr 00:50:56:24:ec:bf STALE
    10.2.1.11 dev ens34 lladdr 00:50:56:33:ac:13 STALE
    ```
    ```
    marcel$ ip n s

    10.2.2.1 dev ens34 lladdr 00:50:56:c0:00:03 DELAY
    10.2.2.254 dev ens34 lladdr 00:0c:29:93:cb:0f STALE
    ```

- essayez de déduire un peu les échanges ARP qui ont eu lieu

    `node1` va d'abord ping `router` qui va ensuite redirigé ce ping vers `marcel`.

- répétez l'opération précédente (vider les tables, puis ping), en lançant tcpdump sur les 3 noeuds, afin de capturer les échanges depuis les 3 points de vue

	```
	$ sudo ip neigh flush all
	```
	```
	$ ping 10.2.1.12
	```
    ```
    $ sudo tcpdump -i ens34 -w [node1/marcel].pcap not port 22
    ```

    [📁 Capture réseau tp2_routage_node1.pcap](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP2/tp2_routage_node1.pcap) [tp2_routage_marcel.pcap](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP2/tp2_routage_marcel.pcap)
    
### 3. Accès internet

#### Donnez un accès internet à vos machines

- ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2

    On commence par activer le masquerading : 

    ```
    $ sudo firewall-cmd --add-masquerade --zone=public
    $ sudo firewall-cmd --add-masquerade --zone=public --permanent
    ```

    Ensuite il suffit d'ajouter la gateway dans /etc/sysconfig/network

    ```
    $ sudo nano /etc/sysconfig/network
    ```
    ```
    GATEWAY=10.2.1.254
    ```

    - vérifiez que vous avez accès internet avec un ping
    
        ```
        node1$ ping 8.8.8.8

        PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
        64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=60.5 ms
        64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=51.9 ms
        ```
        ```
        marcel$ ping 8.8.8.8
        
        PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
        64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=39.8 ms
        64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=37.5 ms
        ```

- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser

    - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`

        ```
        marcel$ dig google.com

        ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
        ;; global options: +cmd
        ;; Got answer:
        ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 19168
        ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

        ;; OPT PSEUDOSECTION:
        ; EDNS: version: 0, flags:; udp: 1232
        ;; QUESTION SECTION:
        ;google.com.                    IN      A

        ;; ANSWER SECTION:
        google.com.             247     IN      A       216.58.198.206

        ;; Query time: 60 msec
        ;; SERVER: 1.1.1.1#53(1.1.1.1)
        ;; WHEN: Thu Sep 23 22:15:25 CEST 2021
        ;; MSG SIZE  rcvd: 55
        ```
        
    - puis avec un ping vers un nom de domaine

        ```
        node1$ ping google.com
        PING google.com (216.58.198.206) 56(84) bytes of data.
        64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=1 ttl=127 time=39.7 ms
        64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=2 ttl=127 time=35.6 ms
        ```
        
#### Analyse de trames

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |
|-------|------------|---------------------|--------------------------|----------------|-----------------|
| 1     | ping       | `node1` `10.2.1.11` |`00:50:56:33:ac:13`       | `8.8.8.8`      |`00:50:56:31:95:42`|
| 2     | pong       | `8.8.8.8`           |`00:50:56:31:95:42`       | `10.2.1.11`    |`00:50:56:33:ac:13`|

[📁 Capture réseau tp2_routage_internet.pcap](https://gitlab.com/esteban47150/tp-reseaux-b2/-/blob/main/TP2/tp2_routage_internet.pcap)

## III. DHCP

### 1. Mise en place du serveur DHCP

#### Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP.

- installation du serveur sur node1.net1.tp2

    ```
    $ sudo dnf install dhcp-server
    ```
    ```
    $ sudo nano /etc/dhcp/dhcpd.conf
    ```
    ```
    default-lease-time 900;
    max-lease-time 10800;
    ddns-update-style none;
    authoritative;
    subnet 10.2.1.0 netmask 255.255.255.0 {
      range 10.2.1.13 10.2.1.100;
      option routers 10.2.1.254;
      option subnet-mask 255.255.255.0;
      option domain-name-servers 1.1.1.1;
    }
    ```
    ```
    $ sudo systemctl start dhcpd
    $ sudo systemctl enable dhcpd
    ```
    ```
    $ sudo systemctl status dhcpd

    ● dhcpd.service - DHCPv4 Server Daemon
       Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
       Active: active (running) since Sat 2021-09-25 14:03:48 CEST; 3min 19s ago
       [...]
    ```
    
- faites lui récupérer une IP en DHCP à l'aide de votre serveur

    ```
    $ sudo nmcli connection modify ens34 ipv4.method auto
    ```
    ```
    $ sudo nmcli connection down ens34; sudo nmcli connection up ens34
    ```

#### Améliorer la configuration du DHCP

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP : 

    - une route par défaut
    - un serveur DNS à utiliser
    
    Je l'ai déjà configuré
    
- récupérez de nouveau une IP en DHCP sur `node2.net1.tp2 `pour tester : 
    - `node2.net1.tp2` doit avoir une IP
        - vérifier avec une commande qu'il a récupéré son IP

            ```
            $ ip a

            [...]
            2: ens34: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
                link/ether 00:50:56:27:41:51 brd ff:ff:ff:ff:ff:ff
                inet 10.2.1.13/24 brd 10.2.1.255 scope global dynamic noprefixroute ens34
            ```
        - vérifier qu'il peut `ping` sa passerelle
            ```
            $ ping 10.2.1.254

            PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
            64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.41 ms
            64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.99 ms
            ```
        
    - il doit avoir une route par défaut
        - vérifier la présence de la route avec une commande
        
            ```
            $ ip r s

            default via 10.2.1.254 dev ens34 proto dhcp metric 100
            10.2.1.0/24 dev ens34 proto kernel scope link src 10.2.1.12 metric 100
            10.2.1.0/24 dev ens34 proto kernel scope link src 10.2.1.13 metric 100
            ```
            
        - vérifier que la route fonctionne avec un `ping` vers une IP
        
            ```
            $ ping 8.8.8.8
            
            PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
            64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=17.9 ms
            64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=19.4 ms
            ```
            
    - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    
        - vérifier avec la commande `dig` que ça fonctionne

            ```
            $ dig google.com

            ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
            ;; global options: +cmd
            ;; Got answer:
            ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59054
            ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

            ;; OPT PSEUDOSECTION:
            ; EDNS: version: 0, flags:; udp: 1232
            ;; QUESTION SECTION:
            ;google.com.                    IN      A

            ;; ANSWER SECTION:
            google.com.             129     IN      A       172.217.18.206

            ;; Query time: 16 msec
            ;; SERVER: 1.1.1.1#53(1.1.1.1)
            ;; WHEN: Sat Sep 25 14:20:56 CEST 2021
            ;; MSG SIZE  rcvd: 55
            ```
            
        - vérifier un `ping` vers un nom de domaine

            ```
            $ ping google.com
            
            PING google.com (172.217.18.206) 56(84) bytes of data.
            64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=1 ttl=127 time=13.10 ms
            64 bytes from ham02s14-in-f206.1e100.net (172.217.18.206): icmp_seq=2 ttl=127 time=16.4 ms
            ```
            
            
---

#### Esteban MARTINEZ - B2B
